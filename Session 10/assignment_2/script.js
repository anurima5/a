function sum(arr)
{

var min_sum;
var min_i;
var min_j;
min_sum= arr[0]+arr[1];
min_i=0;
min_j=1;

    for(var i=0; i<arr.length-1; i++)
    {
        for(var j=i+1; j<arr.length; j++)
        {
            var sum= arr[i]+ arr[j];
            if(Math.abs(min_sum) > Math.abs(sum))
            {
                min_sum= sum;
                min_i=i;
                min_j=j;
            }
        }
    }
    console.log("the two elements whose sum is minimum are "+ arr[min_i] + " and " + arr[min_j]);
}
