CREATE DATABASE ResProductions;
USE ResProductions;

CREATE TABLE user_details (
ID INTEGER PRIMARY KEY,
FirstName TEXT,
LastName TEXT,
Gender TEXT,
DOB DATE,
UserType TEXT,
Address TEXT,
PhoneNumber INTEGER);

INSERT INTO user_details VALUES(1,"a", "b", "F", '1997-11-05', "Premium user" , "street A" , 999999);
INSERT INTO user_details VALUES(2,"c", "d", "M", '1998-08-02', "Normal user" , "street D" , 988888); 

CREATE TABLE book_details (
ID INTEGER PRIMARY KEY,
BookTitle TEXT,
BookAuthor TEXT,
BookPublisher TEXT,
BookGenre TEXT,
ReleaseYear YEAR,
Rating INTEGER,
created_on DATE, 
modified_on DATE, 
created_by TEXT, 
modified_by TEXT, 
statusdetail INTEGER);

INSERT INTO book_details VALUES(1, "Harry Potter 1", "J K Rowling","pottermore", "Fiction", 1997, 8, '1990-10-05', '2020-03-03',"X", "Y", 1);
INSERT INTO book_details VALUES(2, "Harry Potter 2", "J K Rowling","pottermore", "Fiction", 1998, 9,'1990-11-25', '2020-04-05',"W", "G", 1);
INSERT INTO book_details VALUES(3, "Harry Potter 3", "J K Rowling","pottermore", "Fiction", 1999, 7, '2000-10-15', '2020-03-03',"Z", "F", 1);

CREATE TABLE author_details (
ID INTEGER PRIMARY KEY,
AuthorName TEXT,
DOB DATE,
BookPublisher TEXT,
DescriptionDetail TEXT);
INSERT INTO author_details VALUES(1, "J K Rowling", '1997-05-05', "scholastic press", "Good");
INSERT INTO author_details VALUES(2, "J K Rowling", '1998-04-07', "pottermore", "Good");

CREATE TABLE publisher_details (
ID INTEGER PRIMARY KEY,
PublisherName TEXT,
EstablishedOn DATE,
Address TEXT,
DescriptionDetail TEXT);

INSERT INTO author_details VALUES(1,"initail",'1997-06-26', "England", "thriller");
INSERT INTO author_details VALUES(2,"pottermore",'1998-04-07', "England", "fantasy");


ALTER TABLE user_details
ADD created_on DATE, 
ADD modified_on TIMESTAMP, 
ADD created_by TIMESTAMP, 
ADD modified_by TEXT, 
ADD statusdetail INTEGER;

    
 ALTER TABLE author_details
ADD created_on DATE, 
ADD modified_on DATE, 
ADD created_by TEXT, 
ADD modified_by TEXT, 
ADD statusdetail INTEGER;   
    
ALTER TABLE publisher_details
ADD created_on DATE, 
ADD modified_on DATE, 
ADD created_by TEXT, 
ADD modified_by TEXT, 
ADD statusdetail INTEGER;    
    
INSERT INTO author_details VALUES(3, "William Shakespeare", '1950-08-10', "Simon & Schuster", "romantic");

INSERT INTO publisher_details VALUES(3, "Simon & Schuster", '1990-05-06', "wallstreet", "thriller pubisher");

INSERT INTO publisher_details VALUES(3, "Alpha Publication", '1990-05-06', "main road street", "historic pubisher");

INSERT INTO book_details VALUES(4, "After", "Anna Todd", "anna", "Romance", 2014, 10,'2019-06-09', '2018-05-10',"C", "D", 1);

INSERT INTO user_details VALUES(3,"SIA","S", "F", '1999-01-08', "Premium user" , "STREET B New york" , 998989);

SELECT FirstName, LastName FROM user_details 
  WHERE UserType = "Premium User";
  
SELECT FirstName, LastName FROM user_details 
  WHERE Gender= "F";
  
SELECT BookGenre FROM book_details;

SELECT BookTitle  FROM book_details WHERE Rating> 4;

SELECT BookTitle, MAX(Rating) FROM book_details; 

SELECT BookTitle, MIN(Rating) FROM book_details; 

SELECT AuthorName FROM author_details WHERE AuthorName LIKE 'AR%';

SELECT PublisherName FROM publisher_details WHERE EstablishedOn < '2012-01-01';
SELECT BookTitle
FROM book_details
ORDER BY Rating DESC
LIMIT 5;
SELECT BookTitle FROM book_details WHERE ReleaseYear BETWEEN 2012 AND 2018;
SELECT AuthorName FROM author_details WHERE AuthorName="Anna Todd";






